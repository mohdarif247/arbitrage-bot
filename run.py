from deribit_api import RestClient
import bitmex
import logging
import datetime
import time

# List of exchanges to compare
EXCHANGES = ["Deribit", "Bitrex"]

logging.basicConfig(filename='arbitrage.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s')


# Connect to Bitmex and Deribit
def connect():
    bitmex_key = "70smvpv_VToWMgy4bBKcmORQ"
    bitmex_secret = "SuRwbRJhMOQk-Z-bd8Ke2wd0z65-w7kesfE06B4yUen_1waL"
    deribit_key = "4TBqr8rawXZvR"
    deribit_secret = "BPR6AXC2DYWC5JU2IP4YS4MUHQTSCRVO"
    try:
        bitmex_client = bitmex.bitmex(test=False, api_key=bitmex_key, api_secret=bitmex_secret)
        deribit_client = RestClient(deribit_key, deribit_secret)
        return deribit_client, bitmex_client
    except Exception:
        return 0


# Get max bid price and min ask price from deribit
def deribit_exchange(client):
    order_book = client.getorderbook('BTC-28SEP18')
    bid_price = max(list(map(lambda price:price["price"], order_book["bids"])))
    ask_price = min(list(map(lambda price:price["price"], order_book["asks"])))
    return (ask_price, bid_price)


# Get bid price and ask price from bitmex
def bitmex_exchange(client):
    ask_price = float(client.Instrument.Instrument_get(symbol='XBTU18').result()[0][0]["askPrice"]) # Order_get(symbol='XBTUSD').result()[0][0]
    bid_price = float(client.Instrument.Instrument_get(symbol='XBTU18').result()[0][0]["bidPrice"])  # Order_get(symbol='XBTUSD').result()[0][0]
    return (ask_price, bid_price)


# Compare min ask price and max bid price from both exchanges and return lowest ask and highest bid
def cal_best_rate(asks, bids):
    lowest_ask = float('inf')
    lowest_ask_index = -1
    highest_bid = 0
    highest_bid_index = -1
    for i in range(len(EXCHANGES)):
        if asks[i] != 0:
            if asks[i] < lowest_ask:
                lowest_ask = asks[i]
                lowest_ask_index = i
            if bids[i] > highest_bid:
                highest_bid = bids[i]
                highest_bid_index = i
    return lowest_ask, highest_bid, lowest_ask_index, highest_bid_index


# Condition to check Arbitrage
def check_arbitrage(deribit_client, bitmex_client):
    deribit_r = deribit_exchange(deribit_client)
    bitmex_r = bitmex_exchange(bitmex_client)
    l_ask, h_bid, l_index, h_index = cal_best_rate([deribit_r[0], bitmex_r[0]], [deribit_r[1], bitmex_r[1]])
    if l_index != -1 and h_index != -1:
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print("--------------------", current_time, "--------------------------")
        logging.info("--------------------" + str(current_time) + "--------------------------")
        if l_ask == h_bid or l_index == h_index:
            print("No Arbitrage")
            logging.info("No Arbitrage")
        else:
            print("Buy from %s at $ %g" % (EXCHANGES[l_index], l_ask))
            print("Sell on %s at $ %g" % (EXCHANGES[h_index], h_bid,))
            profit = ((h_bid - l_ask) / h_bid) * 100
            print("Profit %g percent on BTC" % (profit))
            logging.info("Buy from %s at $ %g " % (EXCHANGES[l_index], l_ask))
            logging.info("Sell on %s at $ %g" % (EXCHANGES[h_index], h_bid))
            logging.info("Profit %g percent on BTC" % (profit))
        print("------------------------------------------------------------------------")
        logging.info("-----------------------------------------------------------------")


if __name__ == "__main__":
    if connect() == 0:
        print("Connection to Exchange failed")
    else:
        d_client, b_client = connect()
        while True:
            check_arbitrage(d_client, b_client)     # Get ask price and bid price from Deribit and Bitmex every 5 sec
            time.sleep(5)

